﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaEntityFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numero = new List<int>() { 1, 2, 3, 4, 5 };

            var lista = numero;

            foreach (var item in lista)
            {
                Console.WriteLine(item);
            }
        }
    }
}
